import React from "react";
import img2 from "../assets/house12.jpg";
// import img2 from "../assets/img2.jpg";
import img3 from "../assets/croped.png";
import img1 from "../assets/img1.jpg";
import man from "../assets/man.png";
import house1 from "../assets/house1.jpg";
import house3 from "../assets/house3.jpg";
import house4 from "../assets/house4.jpg";
import house6 from "../assets/house6.jpg";
import house7 from "../assets/house7.jpg";
import house8 from "../assets/house8.jpg";
import house10 from "../assets/house10.jpg";

const ProjectsPage = () => {
  const myStyle = {
    backgroundImage: `url(${img2})`,

    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
  };
  return (
    <div>
      <div>
        <div
          className="py-12 justify-center items-center w-full flex flex-row gap-4 min-h-[300px]"
          style={myStyle}
        >
          <div className="border-1 border-b border-gray-100 w-[150px]" />
          <h1 className="text-3xl font-bold text-white">Our Services</h1>
          <div className="border-1 border-b border-gray-100 w-[150px]" />
        </div>
        <div className="flex flex-col justify-start items-center gap-8 py-12">
              <h1 className="text-3xl text-blue-900 font-semibold">
               Enhance Accountability
              </h1>
              <p className="font-light text-gray-400 sm:px-[100px] px-0">
                We take the guesswork out of tenant selection by conducting
                thorough background and credit checks. Our goal is to find
                reliable and responsible tenants for your property. We take the
                guesswork out of tenant selection by conducting thorough
                background and credit checks. Our goal is to find reliable and
                responsible tenants for your property. We take the guesswork out
                of tenant selection by conducting thorough background and credit
                checks. Our goal is to find reliable and responsible tenants for
                your property.
              </p>
            </div>
        <div className="container mx-auto p-2">
          <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-3 gap-8">
            <div className="flex flex-col gap-3 items-center justify-center">
              <h1 className="py-3 text-2xl font-light">Search Residence</h1>
              <img src={house1} className="" alt="" />
              <p className="font-light text-gray-400">
                We conduct regular inspections to ensure your property is in top
                condition, identify potential issues and address them before
                they become major problems.
              </p>
            </div>
            <div className="flex flex-col gap-3 items-center justify-center">
              <h1 className="py-3 text-2xl font-light">Tenant Follow up</h1>
              <img src={img1} className="" alt="" />
              <p className="font-light text-gray-400">
                We take the guesswork out of tenant selection by conducting
                thorough background and credit checks. Our goal is to find
                reliable and responsible tenants for your property.
              </p>
            </div>
            <div className="flex flex-col gap-3 items-center justify-center">
              <h1 className="py-3 text-2xl font-light">Rent Collection</h1>
              <img src={house8} className="" alt="" />
              <p className="font-light text-gray-400">
                We handle rent collection and ensure that payments are made on
                time. We also provide detailed financial reporting so you always
                know where your money is going.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default ProjectsPage;
