import React from "react";
import prog from "../assets/prog.png";
import house8 from "../assets/kev.jpg";
import house6 from "../assets/house6.jpg";
import naf from "../assets/naf.jpeg";
import house1 from "../assets/profile_pic.jpg";
import img1 from "../assets/jess.jpeg";

const AboutPage = () => {
  return (
    <div>
      <div className="py-12 justify-center items-center w-full flex flex-row gap-4 ">
        <div className="border-1 border-b border-gray-900 w-[150px]" />
        <h1 className="text-3xl font-bold text-blue-900">About Us</h1>
        <div className="border-1 border-b border-gray-900 w-[150px]" />
      </div>
      <p className="font-light text-gray-400 sm:px-[100px] px-0 py-12">
            We take the guesswork out of tenant selection by conducting thorough
            background and credit checks. Our goal is to find reliable and
            responsible tenants for your property. We take the guesswork out of
            tenant selection by conducting thorough background and credit
            checks. Our goal is to find reliable and responsible tenants for
            your property. We take the guesswork out of tenant selection by
            conducting thorough background and credit checks. Our goal is to
            find reliable and responsible tenants for your property.
          </p>
      <div className="flex sm:flex-row flex-col items-center">
        <div className="flex flex-col items-center gap-8">
          <h1 className="text-3xl text-blue-900 font-semibold">About REMS</h1>
          <p className="font-light text-gray-400 sm:px-[100px] px-0">
            We take the guesswork out of tenant selection by conducting thorough
            background and credit checks. Our goal is to find reliable and
            responsible tenants for your property. We take the guesswork out of
            tenant selection by conducting thorough background and credit
            checks. Our goal is to find reliable and responsible tenants for
            your property. We take the guesswork out of tenant selection by
            conducting thorough background and credit checks. Our goal is to
            find reliable and responsible tenants for your property.
          </p>
        </div>
        {/* <img src={prog} alt="" className="w-[200px] sm:w-[400px] px-3" /> */}
      </div>
      <div className="flex sm:flex-row flex-col items-center mt-12 py-3">
        <div className="flex flex-col justify-start items-center gap-8">
          <h1 className="text-3xl text-blue-900 font-semibold">Members</h1>
          <p className="font-light text-gray-400 sm:px-[100px] px-0">
            We take the guesswork out of tenant selection by conducting thorough
            background and credit checks. Our goal is to find reliable and
            responsible tenants for your property. We take the guesswork out of
            tenant selection by conducting thorough background and credit
            checks. Our goal is to find reliable and responsible tenants for
            your property. We take the guesswork out of tenant selection by
            conducting thorough background and credit checks. Our goal is to
            find reliable and responsible tenants for your property.
          </p>
        </div>
        {/* <img src={prog} alt="" className="w-[200px] sm:w-[400px] px-3" /> */}
      </div>
      <div className="container mx-auto p-2">
        {" "}
        <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-3 gap-8">
          {" "}
          <div className="flex flex-col gap-3 items-center justify-center">
            <img
              src={house1}
              className="w-[200px] h-[200px] object-cover rounded-full"
              alt=""
            />
            <a href="" className="flex flex-col justify-center items-center">
              <h1 className="py- text-2xl font-light">Don Collins</h1>
              <h1 className="py- text-md font-light text-[#001b5e]/70 cursor-pointer hover:underline duration-150 ease-in">Founder & C.E.O</h1>
             


            <p className="font-light text-gray-400">
              We conduct regular inspections to ensure your property is in top
              condition, identify potential issues and address them before they
              become major problems.
            </p>
            <button className="bg-slate-200 p-1 m-2 rounded-md text-md text-gray-500">View more</button>
            </a>
          </div>
          <div className="flex flex-col gap-3 items-center justify-center">
            <img
              src={img1}
              className="w-[200px] h-[200px] object-cover rounded-full"
              alt=""
            />
            <a href="https://my-portfolio-pnjg.onrender.com" className="flex flex-col justify-center items-center">
              <h1 className="py- text-2xl font-light">Ogola Jesse</h1>
              <h1 className="py- text-md font-light text-[#001b5e]/70 hover:underline duration-150 ease-in">Co-founder & Lead UI/UX Designer</h1>
       

            <p className="font-light text-gray-400">
              We take the guesswork out of tenant selection by conducting
              thorough background and credit checks. Our goal is to find
              reliable and responsible tenants for your property.
            </p>
            <button className="bg-slate-200 p-1 m-2 rounded-md text-md text-gray-500">View more</button>
            </a>
          </div>
      
          <div className="flex flex-col gap-3 items-center justify-center">
            <img
              src={naf}
              className="w-[200px] h-[200px] object-cover rounded-full"
              alt=""
            />
          <a href="https://nafhomes.onrender.com/" className="flex flex-col justify-center items-center">
              <h1 className="py- text-2xl font-light ">Nancy Nafula</h1>
              <h1 className="py- text-md font-light text-[#001b5e]/70 hover:underline duration-150 ease-in">Real Estate Expert</h1>
        

            <p className="font-light text-gray-400">
              We handle rent collection and ensure that payments are made on
              time. We also provide detailed financial reporting so you always
              know where your money is going.
            </p>
            <button className="bg-slate-200 p-1 m-1 rounded-md text-md text-gray-500">View more</button>
            </a >
          </div>
        </div>
      </div>
    </div>
  );
};
export default AboutPage;
