import React from "react";
import img2 from "../assets/img2.jpg";
import img3 from "../assets/croped.png";
import img1 from "../assets/img1.jpg";
import { testimonials } from "../constants";
import { IoIosArrowBack, IoIosArrowForward } from "react-icons/io";
import man from "../assets/man.png";
import house1 from "../assets/house1.jpg";
import house4 from "../assets/house4.jpg";
import house6 from "../assets/house6.jpg";
import house7 from "../assets/house7.jpg";
import house8 from "../assets/house8.jpg";
import house10 from "../assets/house10.jpg";
import { Button, Select, TextField } from "@mui/material";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormHelperText from "@mui/material/FormHelperText";
import FormControl from "@mui/material/FormControl";
import { MdArrowDropDown } from "react-icons/md";
import Feedback from "../components/Feedback";
import { FaFacebookF, FaInstagram, FaXTwitter } from "react-icons/fa6";
import { IoLogoYoutube } from "react-icons/io";
import Footer from "../components/Footer";
import { useNavigate } from "react-router-dom";
import TextSlider from "../components/TextSlider";



const HomePage = () => {

  const myStyle = {
    backgroundImage: `url(${img2})`,

    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
  };
  const [age, setAge] = React.useState("");
  const navigate = useNavigate()

  const handleChange = (event) => {
    setAge(event.target.value);
  };
  const handleNavigate_contacts = () =>{

    navigate("/contacts")
  }
  const slides = [

    {
      testimonial:
        "I've been an avid gamer for years, and I can confidently say that We Have The Power is a masterpiece!  and I can't get enough of the thrilling quests and challenges.",
      name: "Sara Atieno",
      designation: "CFO",
      company: "Acme Co",
      image: "https://randomuser.me/api/portraits/women/4.jpg",
    },
    {
      testimonial:
        " The graphics are breathtaking, the storyline is immersive, and the gameplay is smooth and addictive.",
      name: "Jackton Mwangeli",
      designation: "COO",
      company: "DEF Corp",
      image: "https://randomuser.me/api/portraits/men/5.jpg",
    },
    {
      testimonial:
        " It's truly a gaming experience like no other! I've lost track of time while exploring its vast open world,",
      name: "Lisa Mureithi",
      designation: "CTO",
      company: "456 Enterprises",
      image: "https://randomuser.me/api/portraits/women/6.jpg",
    },
  ];

  const containerStyles = {
    height:"30%",
    width: "100%", // Changed "full" to "100%"
    
  };


  return (
    <>
      <div className="">
        <div
          className="w-screen relative flex flex-col items-center  min-h-[900px] "
          style={myStyle}
        >
          <div className=" flex flex-col justify-center items-center gap-4">
            <h1 className="py-4 justify-center font-black sm:text-7xl text-4xl text-white/90 mt-6">
              Welcome to REMS
            </h1>

            <p className="sm:px-[100px] px-[2px] text-white text-lg font-bold py-1 mx-auto">
              Residential and Commercial Property Management Software solution
              designed specifically to help Real Estate Developers and Agents to
              work more efficiently and effectively in Monitoring there sales
              and tracking there sales progress.
            </p>
          </div>
     
            
            <div className="py-12 justify-center items-center w-full flex flex-row gap-4 mt-[200px]">
            <div className="border-1 border-b border-gray-100 w-[200px]" />
            <h1 onClick={handleNavigate_contacts} className="text-2xl text-gray-100 hover:bg-slate-400 duration-150 ease-in bg-gray-500 rounded-full px-6 cursor-pointer p-1">Online Appointments</h1>
            <div className="border-1 border-b border-gray-100 w-[200px]" />
          </div>
        </div>
        <div className=" px-10">
        <div className="py-8 flex sm:flex-row flex-col items-center gap-3 justify-center">
            <FormControl sx={{ m: 1, minWidth: 320 }}>
              <InputLabel id="demo-simple-select-helper-label">
                I would like to register as{" "}
              </InputLabel>
              <Select
                labelId="demo-simple-select-helper-label"
                id="demo-simple-select-helper"
                value={age}
                label="Age"
                onChange={handleChange}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={10}>Agent</MenuItem>
                <MenuItem value={20}>Developer</MenuItem>
                <MenuItem value={30}>Landlord</MenuItem>
              </Select>
              <FormHelperText>With label + helper text</FormHelperText>
            </FormControl>
            <Button variant="contained" color="error">
              Get Started
            </Button>
          </div>
          <div className="py-12 justify-center items-center w-full flex flex-row gap-4 ">
            <div className="border-1 border-b border-gray-900 w-[200px]" />
            <h1 className="sm:text-3xl text-xl text-blue-900">
            Redefining Real Estate Management
            </h1>
            <div className="border-1 border-b border-gray-900 w-[200px]" />
       
          </div>
          <p className="font-light text-gray-400 sm:px-[100px] px-0">
                We take the guesswork out of tenant selection by conducting
                thorough background and credit checks. Our goal is to find
                reliable and responsible tenants for your property. We take the
                guesswork out of tenant selection by conducting thorough
                background and credit checks. Our goal is to find reliable and
                responsible tenants for your property. We take the guesswork out
                of tenant selection by conducting thorough background and credit
                checks. Our goal is to find reliable and responsible tenants for
                your property.
              </p>
          <div className="flex sm:flex-row flex-col items-center">
            <div className="flex flex-col items-center gap-8">
              <h1 className="text-3xl text-blue-900 font-semibold">
                Management Services
              </h1>
              <p className="font-light text-gray-400 sm:px-[100px] px-0">
                We take the guesswork out of tenant selection by conducting
                thorough background and credit checks. Our goal is to find
                reliable and responsible tenants for your property. We take the
                guesswork out of tenant selection by conducting thorough
                background and credit checks. Our goal is to find reliable and
                responsible tenants for your property. We take the guesswork out
                of tenant selection by conducting thorough background and credit
                checks. Our goal is to find reliable and responsible tenants for
                your property.
              </p>
            </div>
            <img src={man} alt="" className="w-[200px] sm:w-[400px]" />
          </div>
        </div>
        <div className="py-12 justify-center items-center w-full flex flex-row gap-4 ">
          <div className="border-1 border-b border-gray-900 w-[200px]" />
          <h1 className="text-3xl text-blue-900">
            Expert Property Management Services
          </h1>
          <div className="border-1 border-b border-gray-900 w-[200px]" />
        </div>
        <div className="container mx-auto p-2">
          <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-3 gap-8">
            <div className="flex flex-col gap-3 items-center justify-center">
              <h1 className="py-3 text-2xl font-light">Search Residence</h1>
              <img src={house1} className="" alt="" />
              <p className="font-light text-gray-400">
                We conduct regular inspections to ensure your property is in top
                condition, identify potential issues and address them before
                they become major problems.
              </p>
            </div>
            <div className="flex flex-col gap-3 items-center justify-center">
              <h1 className="py-3 text-2xl font-light">Tenant Follow up</h1>
              <img src={img1} className="" alt="" />
              <p className="font-light text-gray-400">
                We take the guesswork out of tenant selection by conducting
                thorough background and credit checks. Our goal is to find
                reliable and responsible tenants for your property.
              </p>
            </div>
            <div className="flex flex-col gap-3 items-center justify-center">
              <h1 className="py-3 text-2xl font-light">Rent Collection</h1>
              <img src={house8} className="" alt="" />
              <p className="font-light text-gray-400">
                We handle rent collection and ensure that payments are made on
                time. We also provide detailed financial reporting so you always
                know where your money is going.
              </p>
            </div>
            <div className="flex flex-col gap-3 items-center justify-center">
              <h1 className="py-3 text-2xl font-light">Search Residence</h1>
              <img src={house1} className="" alt="" />
              <p className="font-light text-gray-400">
                We conduct regular inspections to ensure your property is in top
                condition, identify potential issues and address them before
                they become major problems.
              </p>
            </div>
            <div className="flex flex-col gap-3 items-center justify-center">
              <h1 className="py-3 text-2xl font-light">Tenant Follow up</h1>
              <img src={img1} className="" alt="" />
              <p className="font-light text-gray-400">
                We take the guesswork out of tenant selection by conducting
                thorough background and credit checks. Our goal is to find
                reliable and responsible tenants for your property.
              </p>
            </div>
            <div className="flex flex-col gap-3 items-center justify-center">
              <h1 className="py-3 text-2xl font-light">Rent Collection</h1>
              <img src={house8} className="" alt="" />
              <p className="font-light text-gray-400">
                We handle rent collection and ensure that payments are made on
                time. We also provide detailed financial reporting so you always
                know where your money is going.
              </p>
            </div>
          
          </div>

          {/* <Feedback /> */}
          <div style={containerStyles}>
        <TextSlider slides={slides} />
      </div>

        </div>
      </div>
    </>
  );
};
export default HomePage;
