import React, { useState, useEffect } from "react";
import { IoIosArrowBack, IoIosArrowForward } from "react-icons/io";
import { MdBrightness1 } from "react-icons/md";
import { MdGames } from "react-icons/md";
import { IoLogoGameControllerB } from "react-icons/io";
import { BiSlideshow } from "react-icons/bi";
import { styles } from "../styles";
import { fadeIn, textVariant } from "../utils/motion";
import { motion } from "framer-motion";

const slideStyles = {
  width: "100%",
  height: "100%",
  backgroundSize: "cover",
  backgroundPosition: "center",
  transition: "opacity 0.5s ease-in-out",
};
const testimonials = [
  {
    testimonial:
      "I've been an avid gamer for years, and I can confidently say that We Have The Power is a masterpiece!  and I can't get enough of the thrilling quests and challenges.",
    name: "Sara Atieno",
    designation: "CFO",
    company: "Acme Co",
    image: "https://randomuser.me/api/portraits/women/4.jpg",
  },
  {
    testimonial:
      " The graphics are breathtaking, the storyline is immersive, and the gameplay is smooth and addictive.",
    name: "Jackton Mwangeli",
    designation: "COO",
    company: "DEF Corp",
    image: "https://randomuser.me/api/portraits/men/5.jpg",
  },
  {
    testimonial:
      " It's truly a gaming experience like no other! I've lost track of time while exploring its vast open world,",
    name: "Lisa Mureithi",
    designation: "CTO",
    company: "456 Enterprises",
    image: "https://randomuser.me/api/portraits/women/6.jpg",
  },
];

const rightArrowStyles = {
  position: "absolute",
  top: "50%",
  transform: "translate(0, -50%)",
  right: "32px",
  color: "#1111",
  zIndex: 1,
  cursor: "pointer",
};

const leftArrowStyles = {
  position: "absolute",
  top: "50%",
  transform: "translate(0, -50%)",
  left: "32px",
  color: "#1111",
  zIndex: 1,
  cursor: "pointer",
};

const sliderStyles = {
  position: "relative",
  height: "100%",
  overflow: "hidden",
};

const dotsContainerStyles = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "center",
  position: "absolute",
  right: "50%",
  bottom: "10%",
  gap: "2rem",
};

const dotStyle = {
  margin: "0 3px",
  cursor: "pointer",
  fontSize: "20px",
};

const icons = [
  <MdBrightness1 size={24} />,
  <MdGames size={24} />,
  <IoLogoGameControllerB size={24} />,
]; // Array of icons

const TextSlider = ({ slides }) => {
  const [currentIndex, setCurrentIndex] = useState(0);
  const [activeDot, setActiveDot] = useState(null);

  // Function to go to the next slide
  const goToNext = () => {
    const isLastSlide = currentIndex === slides.length - 1;
    const newIndex = isLastSlide ? 0 : currentIndex + 1;
    setCurrentIndex(newIndex);
  };

  useEffect(() => {
    const interval = setInterval(goToNext, 6000); // Slide every 1 second

    return () => clearInterval(interval); // Cleanup function
  }, [currentIndex]);

  const goToSlide = (slideIndex) => {
    setCurrentIndex(slideIndex);
    setActiveDot(slideIndex);
  };

  // const slideStylesWidthBackground = {
  //   ...slideStyles,
  //   backgroundImage: `url(${slides[currentIndex].url})`,
  // };

  return (
    <div style={sliderStyles}>
      <div className="flex flex-row justify-between">
        <div
          onClick={() =>
            setCurrentIndex(
              currentIndex - 1 >= 0 ? currentIndex - 1 : slides.length - 1
            )
          }
          style={leftArrowStyles}
        >
          <IoIosArrowBack size={24} />
        </div>
        <div
          onClick={() =>
            setCurrentIndex(
              currentIndex + 1 < slides.length ? currentIndex + 1 : 0
            )
          }
          style={rightArrowStyles}
        >
          <IoIosArrowForward size={24} />
        </div>
      </div>
      <div className={`bg-gray-200 shadow-lg rounded-2xl min-h-[300px] `}>
        <motion.div variants={textVariant()} className={styles.paddingX}>
          <p className={`${styles.sectionSubText} p-4`}>What others say</p>
          <h2 className={`text-xl sm:text-5xl font-extrabold text-[#001b5e] `}>
            Testimonials.
          </h2>
          <p className="text-gray-600 font-black text-[48px]">"</p>
        </motion.div>

        <div
          className={`w-full h-full flex mt-12 rounded-[20px] `}
          style={{
            transition: "transform 1s ease, opacity 0.5s ease-in-out",
            transform: `translateX(${-100 * currentIndex}%)`,
          }}
        >
          {testimonials.map((slides, index) => (
            <div
              key={index}
              alt={`slides-${index}`} // Providing a meaningful alt text
              className="object-cover w-full h-full block shrink-0 grow-0 rounded-[20px]"
            >
              <div className="mt-1">
                <p className="text-gray-700 tracking-wider text-[18px] px-6">
                  {slides.testimonial}
                </p>

                <div className="mt-7 flex justify-between items-center gap-1">
                  <div className="flex-1 flex flex-col px-6">
                    <p className="text-gray-600 font-medium text-[16px]">
                      <span className="blue-text-gradient">@</span>{" "}
                      {slides.name}
                    </p>
                    <p className="mt-1 text-secondary text-[12px]">
                      {slides.designation} of {slides.company}
                    </p>
                    <img
                    src={slides.image}
                    alt={`feedback_by-${slides.name}`}
                    className="w-16 h-16 rounded-full object-cover p-2 "
                  />
                  </div>

                
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default TextSlider;
