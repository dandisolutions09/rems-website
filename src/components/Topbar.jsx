
import React, { useEffect, useState } from "react";
import { Link, NavLink } from "react-router-dom";
import { FaFacebookF } from "react-icons/fa6";
import { FaXTwitter } from "react-icons/fa6";
import { IoLogoYoutube } from "react-icons/io";
import { FaInstagram } from "react-icons/fa6";
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import Input from '@mui/material/Input';
import InputLabel from '@mui/material/InputLabel';
import InputAdornment from '@mui/material/InputAdornment';
import FormControl from '@mui/material/FormControl';
import TextField from '@mui/material/TextField';
import AccountCircle from '@mui/icons-material/AccountCircle';
import PhoneAndroidIcon from '@mui/icons-material/PhoneAndroid';
import MailOutlineIcon from '@mui/icons-material/MailOutline';

import {
    AiOutlineMenu,
    AiOutlineHome,
    AiOutlineProject,
    AiOutlineMail,
    AiOutlineMenuFold,
} from "react-icons/ai";
import { GrProjects } from "react-icons/gr";
import { GoProjectRoadmap } from "react-icons/go";

import { BsPerson } from "react-icons/bs";

import { styles } from "../styles";
import { FiPhone } from "react-icons/fi";

const Topbar = () => {
    const [nav, setNav] = useState(false);
    function handleNav() {
        setNav(!nav);
        console.log("set changed");
    }
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    return (
        <div>
            {/* <AiOutlineMenuFold  
                onClick={handleNav}
                color="white"
                className="absolute top-4 right-0 z-[99] md:hidden"
            /> */}
        
              <div className="md:hidden lg:hidden sm:fixed  relative z-10 ">
              <div className="flex flex-row justify-around items-center shadow-lg p-2   bg-[#001b5e] ">
                  <div
                      className="flex justify-center items-center "
                  >
                      <div className="flex flex-col">
                          <div className="flex items-center flex-row ">
                              <FiPhone size={15} color={"gray"} />
                              <span className="pl-2 text-xs text-white font-extralight cursor-pointer hover:scale-110 ease-in duration-300">+44 7771 281254</span>
                          </div>
                          <div className="flex items-center flex-row ">
                              <AiOutlineMail size={15} color={"gray"} />
                              <span className="pl-2 text-xs text-white font-extralight cursor-pointer hover:scale-110 ease-in duration-300">Rems@gmail.com</span>
                          </div>
                      </div>
                  </div>

                  <div
                      className="flex justify-center items-center m-2 p-2 gap-3 "
                  >   <FaXTwitter size={13}  className=" text-white cursor-pointer hover:scale-110 ease-in duration-300 hover:text-blue-600"/>
                      <FaFacebookF size={13}  className=" text-white cursor-pointer hover:scale-110 ease-in duration-300 hover:text-blue-600"/>     
                      <IoLogoYoutube size={13}  className=" text-white cursor-pointer hover:scale-110 ease-in duration-300 hover:text-blue-600"/>
                      <FaInstagram size={13}  className=" text-white cursor-pointer hover:scale-110 ease-in duration-300 hover:text-blue-600"/>

                  </div>

                  <div
                      className="flex justify-center items-center mr-8"
                  >
                      <div className="flex flex-col gap-2">
                          <div onClick={handleOpen} className="flex items-center px-2 flex-row border-2 p-1 rounded-md cursor-pointer hover:scale-110 ease-in duration-300">
                        
                              <span className=" text-xs text-white font-extralight ">Free Trial</span>
                          </div>
                          <div onClick={handleOpen} className="flex items-center px-2 flex-row border-2 p-1 rounded-md hover:scale-110 ease-in duration-300">
                            
                              <span className=" text-xs text-white font-extralight cursor-pointer ">Register</span>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
       
            <div className="md:block hidden  relative z-10"> 
                <div className="flex flex-row justify-around items-center shadow-lg py-2 bg-[#001b5e] ">
                    <div
                        className="flex justify-center items-center "
                    >
                        <div className="flex flex-col">
                            <div className="flex items-center flex-row ">
                                <FiPhone size={20} color={"gray"} />
                                <span className="pl-4 text-sm text-white font-extralight cursor-pointer hover:scale-110 ease-in duration-300">+44 7771 281254</span>
                            </div>
                            <div className="flex items-center flex-row ">
                                <AiOutlineMail size={20} color={"gray"} />
                                <span className="pl-4 text-sm text-white font-extralight cursor-pointer hover:scale-110 ease-in duration-300">rems@gmail.com</span>
                            </div>
                        </div>
                    </div>

                    <div
                        className="flex justify-center items-center m-2 p-4 gap-8 "
                    >   <FaXTwitter size={20}  className=" text-white cursor-pointer hover:scale-110 ease-in duration-300 hover:text-blue-600"/>
                        <FaFacebookF size={20}  className=" text-white cursor-pointer hover:scale-110 ease-in duration-300 hover:text-blue-600"/>     
                        <IoLogoYoutube size={20}  className=" text-white cursor-pointer hover:scale-110 ease-in duration-300 hover:text-blue-600"/>
                        <FaInstagram size={20}  className=" text-white cursor-pointer hover:scale-110 ease-in duration-300 hover:text-blue-600"/>

                    </div>

                    <div
                        className="flex justify-center items-center "
                    >
                        <div className="flex sm:flex-row flex-col gap-3">
                            <div onClick={handleOpen} className="flex items-center px-6 flex-row  border-2 p-2 rounded-md cursor-pointer hover:scale-110 ease-in duration-300">
                          
                                <span className=" text-sm text-white font-extralight ">Free Trial</span>
                            </div>
                            <div onClick={handleOpen} className="flex items-center px-6 flex-row  border-2 p-2 rounded-md hover:scale-110 ease-in duration-300">
                              
                                <span className=" text-sm text-white font-extralight cursor-pointer ">Register</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={mystyle}>
                    <Typography id="modal-modal-title" variant="h5" component="h2" sx={{ fontWeight: "bold", color: "#001b5e" }}>
                        Sign Up to get Free Trial
                    </Typography>
                    {/* <Typography id="modal-modal-description" sx={{ mt: 2 }} className="font-light text-sm">
                        Duis mollis, est non commodo luctus, nisi erat porttitor ligula.
                    </Typography> */}
                    <div className="py-3 flex flex-col gap-3">   
                           <TextField
                        id="input-with-icon-textfield"
                        label="Phone"
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="start">
                                    <PhoneAndroidIcon />
                                </InputAdornment>
                            ),
                        }}
                        variant="outlined"
                        fullWidth
                        helperText="Enter emails address"
                    />
                        <TextField
                            id="input-with-icon-textfield"
                            label="Email"
                            InputProps={{
                                endAdornment: (
                                    <InputAdornment position="start">
                                        <MailOutlineIcon />
                                    </InputAdornment>
                                ),
                            }}
                            variant="outlined"
                            fullWidth
                            helperText="Enter emails address"
                        />
                        <Button variant="contained" sx={{bgcolor:"#001b5e"}} className="bg-[#001b5e] text-white">Sumbmit</Button>
                        </div>
                </Box>
            </Modal>
        </div>
    );
};
const mystyle = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'white',
    borderRadius: 5,
    boxShadow: 1,
    p: 4,
};

export default Topbar;
