import React from "react";
import { FaFacebookF, FaInstagram, FaXTwitter } from "react-icons/fa6";
import { IoLogoYoutube } from "react-icons/io";
import ios from "../assets/ios.png";
import android from "../assets/android.png";

export default function Footer() {
  return (
    <>
      <div className="mt-12 bg-[#001b5e] select-none">
        <div className="p-4 flex flex-row sm:gap-4 gap-0 justify-between">
          <div className="flex flex-col gap-3">
            <h1 className="sm:text-2xl text-xl font-bold text-white">Contact Us</h1>
            <div>
              <span className=" text-sm text-white font-extralight cursor-pointer flex flex-row gap-2">
                <p>Phone:</p>+44 7771 281254
              </span>
              <span className=" text-sm text-white font-extralight cursor-pointer flex flex-row gap-2">
                <p>Email:</p>rems@gmail.com
              </span>
            </div>
          </div>
          <div className="flex flex-col gap-3">
            <h1 className="sm:text-2xl text-xl font-bold text-white">Quick Links</h1>
            <div>
              <span className=" text-sm text-white font-extralight cursor-pointer flex flex-row gap-2 hover:underline duration-100 ease-in">
                FAQS
              </span>
              <span className=" text-sm text-white font-extralight cursor-pointer flex flex-row gap-2">
                Blogs
              </span>
            </div>
          </div>
          <div className="flex flex-col gap-3">
            <h1 className="sm:text-2xl text-xl font-bold text-white">Mobile App</h1>
            <div className="flex flex-col gap-1">
              <span className=" text-sm text-white font-extralight cursor-pointer flex flex-row gap-2">
                <div>
                  <img
                    src={ios}
                    alt=""
                    width={130}
                    className="cursor-pointer hover:scale-110 duration-200 ease-in"
                  />
                </div>
              </span>
              <span className=" text-sm text-white font-extralight cursor-pointer flex flex-row gap-2">
                <div>
                  <img
                    src={android}
                    alt=""
                    width={130}
                    className="cursor-pointer hover:scale-110 duration-200 ease-in"
                  />
                </div>
              </span>
            </div>
          </div>
          <div className="flex flex-col gap-3">
            <h1 className="sm:text-2xl text-xl font-bold text-white">Support</h1>
            <div>
              <span className=" text-sm text-white font-extralight cursor-pointer flex flex-row gap-2">
                <p>Phone:</p>+44 7771 281254
              </span>
              <span className=" text-sm text-white font-extralight cursor-pointer flex flex-row gap-2">
                <p>Email:</p>rems@gmail.com
              </span>
            </div>
          </div>
        </div>

        <div className="flex justify-center items-center m-2 p-4 gap-8 ">
          {" "}
          <FaXTwitter
            size={20}
            className=" text-gray-600 cursor-pointer hover:scale-110 ease-in duration-300 hover:text-blue-600"
          />
          <FaFacebookF
            size={20}
            className=" text-gray-600 cursor-pointer hover:scale-110 ease-in duration-300 hover:text-blue-600"
          />
          <IoLogoYoutube
            size={20}
            className=" text-gray-600 cursor-pointer hover:scale-110 ease-in duration-300 hover:text-blue-600"
          />
          <FaInstagram
            size={20}
            className=" text-gray-600 cursor-pointer hover:scale-110 ease-in duration-300 hover:text-blue-600"
          />
        </div>
        <div className="flex flex-col justify-center items-center relative w-full">
          <div className="justify-center items-center">
            <h1 className="font-bold text-xl text-white">REMS</h1>
          </div>
        </div>
        <p className="text-sm font-light m-4 text-white/50">
          Copyright © 2024 REMS - All Rights Reserved.
        </p>
      </div>
    </>
  );
}
