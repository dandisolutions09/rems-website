import React, { useEffect, useState } from "react";
import { Link, NavLink } from "react-router-dom";

import {
  AiOutlineMenu,
  AiOutlineHome,
  AiOutlineProject,
  AiOutlineMail,
  AiOutlineMenuFold,
} from "react-icons/ai";
import { GrProjects } from "react-icons/gr";
import { GoProjectRoadmap } from "react-icons/go";
import { FiPhone } from "react-icons/fi";

import { BsPerson } from "react-icons/bs";

import { styles } from "../styles";

const Navbar = () => {
  const [nav, setNav] = useState(false);
  function handleNav() {
    setNav(!nav);
    console.log("set changed");
  }

  return (
    <div>
      <AiOutlineMenuFold
        onClick={handleNav}
        className="absolute top-[30px] right-[10px] z-[99] md:hidden text-white"
      />
      {nav ? (
        <div className="fixed  bg-white/90 flex flex-col justify-center items-center z-20">
          <NavLink
            onClick={handleNav}
            to="/"
            className="flex justify-center items-center m-2 p-4 cursor-pointer hover:scale-110 ease-in duration-300"
          >
            <AiOutlineHome size={20} color={"gray"} />
            <span className="pl-4 text-gray-500">Home</span>
          </NavLink>

          <NavLink
            onClick={handleNav}
            to="/projects"
            className="flex justify-center items-center m-2 p-4 cursor-pointer hover:scale-110 ease-in duration-300"
          >
            <GoProjectRoadmap size={20} color={"gray"} />
            <span className="pl-4 text-gray-500">Services</span>
          </NavLink>

          <NavLink
            onClick={handleNav}
            to="/about"
            className="flex justify-center items-center m-2 p-4 cursor-pointer hover:scale-110 ease-in duration-300"
          >
            <BsPerson size={20} color={"gray"} />
            <span className="pl-4 text-gray-500">About Us</span>
          </NavLink>

          <NavLink
            onClick={handleNav}
            to="/members"
            className="flex justify-center items-center m-2 p-4 cursor-pointer hover:scale-110 ease-in duration-300"
          >
            <AiOutlineProject size={20} color={"gray"} />
            <span className="pl-4 text-gray-500">Members</span>
          </NavLink>

          <NavLink
            onClick={handleNav}
            to="/contacts"
            className="flex justify-center items-center m-2 p-4 cursor-pointer hover:scale-110 ease-in duration-300"
          >
            <AiOutlineMail size={20} color={"gray"} />
            <span className="pl-4 text-gray-500">Contact</span>
          </NavLink>
        </div>
      ) : (
        ""
      )}
      <div className="md:block hidden relative z-10">
      <Link  to="/" className="absolute sm:text-xl lg:text-5xl md:text-4xl font-black text-[#001b5e] cursor-pointer top-4 m-4">Rems</Link>

        <div className="flex flex-row justify-end items-center p-3 ">

          <NavLink
            to="/"
            className="flex justify-center items-center m-2 p-4 cursor-pointer hover:scale-110 ease-in duration-300"
          >
            <AiOutlineHome size={20} color={"gray"} />
            <span className="pl-3 text-gray-500">Home</span>
          </NavLink>
          <NavLink
            to="/projects"
            className="flex justify-center items-center m-2 p-4 cursor-pointer hover:scale-110 ease-in duration-300"
          >
            <GoProjectRoadmap size={20} color={"gray"} />
            <span className="pl-3 text-gray-500">Services</span>
          </NavLink>
          <NavLink
            to="/about"
            className="flex justify-center items-center m-2 p-4 cursor-pointer hover:scale-110 ease-in duration-300"
          >
            <BsPerson size={20} color={"gray"} />
            <span className="pl-3 text-gray-500">About Us</span>
          </NavLink>
          {/* <NavLink
            to="/members"
            className="flex justify-center items-center m-2 p-4 cursor-pointer hover:scale-110 ease-in duration-300"
          >
            <AiOutlineProject size={20} color={"gray"} />
            <span className="pl-4 text-gray-500">Members</span>
          </NavLink> */}
          <NavLink
            to="/contacts"
            className="flex justify-center items-center m-2 p-4 cursor-pointer hover:scale-110 ease-in duration-300"
          >
            <AiOutlineMail size={20} color={"gray"} />
            <span className="pl-3 text-gray-500">Contact</span>
          </NavLink>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
