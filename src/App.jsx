import { HashRouter, Route, Routes } from "react-router-dom";
import HomePage from "./pages/HomePage";
import { ThemeProvider, createTheme } from "@mui/material";
import ContactPage from "./pages/ContactPage";
import AboutPage from "./pages/AboutPage";
import Navbar from "./components/Navbar";
import ProjectsPage from "./pages/ProjectsPage";
import MembersPage from "./pages/MembersPage";
import Topbar from "./components/Topbar";
import Footer from "./components/Footer";




function App() {
  const theme = createTheme({
    typography: {
      fontFamily: ["Play"],
      backdropFilter: "none",
    },
  });
  return (
    <ThemeProvider theme={theme}>
      <HashRouter>
      <Topbar/>
        <Navbar/>     
        <Routes>
          <Route index element={<HomePage />} />
          <Route path="/" element={<HomePage />} />
          <Route path="/contacts" element={<ContactPage />} />
          <Route path="/about" element={<AboutPage />} />
          <Route path="/projects" element={<ProjectsPage />} />
          <Route path="/members" element={<MembersPage />} />
        </Routes>
        <Footer/>

      </HashRouter>
    </ThemeProvider>
  );
}

export default App;
